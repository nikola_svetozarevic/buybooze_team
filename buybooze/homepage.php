<?php
/**
 * Template Name: home page
 */
 require_once('inc/particials/navbar.php'); ?>

    <div id="index">
        <div class="container">
            <div class="row" id="big-image" style="background-image: url('<?php echo get_field( 'homepage_image' )['url'];?>')"></div>

            <?php
            $taxonomy     = 'product_cat';
            $orderby      = 'name';
            $show_count   = 0;      // 1 for yes, 0 for no
            $pad_counts   = 0;      // 1 for yes, 0 for no
            $hierarchical = 1;      // 1 for yes, 0 for no
            $title        = '';
            $empty        = 0;

            $args = array(
                'taxonomy'     => $taxonomy,
                'orderby'      => $orderby,
                'show_count'   => $show_count,
                'pad_counts'   => $pad_counts,
                'hierarchical' => $hierarchical,
                'title_li'     => $title,
                'hide_empty'   => $empty
            );
            $all_categories = get_categories( $args );
            foreach ($all_categories as $cat) { ?>
                <div class="row category-row">
                    <div class="col-xs-6 category-title"><?php echo $cat->name; ?> </div>
                    <div class="col-xs-6 category-show-more">
                        <!-- <a href="<?php //echo get_site_url()?>/category-page?category=<?php echo $cat->slug?>">SHOW MORE</a> -->
                        <a href="<?php echo get_permalink(17)?>&category=<?php echo $cat->slug?>">SHOW MORE</a>
                    </div>
                </div>

                <div class="row">
                    <?php
                    $args = array(
                        'post_type'             => 'product',
                        'post_status'           => 'publish',
                        'posts_per_page'        => '4',
                        'product_cat'           => $cat->slug,
                    );

                    $loop = new WP_Query($args);

                    while ( $loop->have_posts() ) : $loop->the_post();
                        global $product;
                        global $woocommerce;
                        $image = wp_get_attachment_image_src( get_post_thumbnail_id( $loop->post->ID ), 'single-post-thumbnail' );
                        ?>

                        <a href="<?php echo get_permalink( $product->id );?>" class="col-sm-3 article">
                            <div class="article-image" style="background-image: url('<?php echo $image[0];?>')"></div>
                            <div class="row">
                                <div class="col-xs-8 article-name"><?php echo $product->name;?></div>
                                <div class="col-xs-4 article-price">$<?php echo $product->price;?></div>
                            </div>
                            <div class="article-desc"><?php echo $product->description;?></div>
                        </a>

                        <?php
                    endwhile; ?>
                </div>
        <?php } ?>


        </div>
    </div>

<?php require_once('inc/particials/footer.php'); ?>