<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="user-scalable=no, initial-scale=1, maximum-scale=1, minimum-scale=1, width=device-width">
    <title>Buy Booze</title>
</head>

<body>
    <?php wp_head();?>
    <!-- NAVBAR -->
    <div id="navbar">
        <div id="navbar-container">
            <div id="logo"><img src="<?php echo get_theme_file_uri();?>/content/images/logo.png" draggable="false"></div>
            <div id="link-wrapper" >

                <?php wp_nav_menu( [ 'theme_location' => 'header-menu' ] );?>

                <div id="search-button" class="navbar-link"><a href="#"><i class="glyphicon glyphicon-search" aria-hidden="true"></i> SEARCH</a></div>
                <div class="navbar-last">
                    <div class="navbar-link"><a href="<?php echo WC_Cart::get_cart_url();?>"><span id="cart-number"><?php  echo WC()->cart->get_cart_contents_count(); ?></span><div class="glyphicon glyphicon-shopping-cart" aria-hidden="true"></div>CART</a></div>
                    <div class="navbar-link"><a href="<?php echo get_permalink( get_page_by_path( 'my-account' ) ); ?>"><div class="glyphicon glyphicon-user" aria-hidden="true"></div>LOGIN</a></div>
                </div>
            </div>
        </div>
    </div>

    <div id="search">
        <form method="get" action="<?php echo get_page_link( 28 )?>" id="search-form">
            <div class="container">
                <div id="search-close-button-wrapper"><div id="search-close-button">CLOSE</div></div>
                <div id="search-title">What are you looking for?</div>
                <input placeholder="SEARCH HERE" id="search-input" name="product_name">
            </div>
        </form>
    </div>

    <div id="loadingScreen">
        <div class="loadingContainer">
            <div class="circle"></div>
        </div>
        <p>LOADING</p>
    </div>

