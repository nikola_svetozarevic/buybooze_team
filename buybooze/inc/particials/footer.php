
    
    <div id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-8 footer-links-left">
                    <div class="col-md-3"><a href="#">Shipping and Returns</a></div>
                    <div class="col-md-3"><a href="#">Terms & Conditions</a></div>
                    <div class="col-md-3"><a href="#">Contact</a></div>
                    <!-- <div class="col-md-3"><a href="#">Newsletter</a></div> -->
                </div>
                <div class="col-md-4 footer-links-right">
                    <div class="col-md-6">FOLLOW US</div>
                    <div class="col-md-6">
                        <a href="#"><img src="<?php echo get_theme_file_uri();?>/content/images/fb.png" draggable="false"></a>
                        <a href="#"><img src="<?php echo get_theme_file_uri();?>/content/images/g+.png" draggable="false"></a>
                        <a href="#"><img src="<?php echo get_theme_file_uri();?>/content/images/p.png" draggable="false"></a>
                    </div>
                </div>
            </div>
            <hr id="line">
            <div class="row">
                <div class="col-md-9 footer-info">
                    <div>Emptyglass © 2017. ABN 80 145 802 544. Liquor licence: LIQP770016698</div>
                </div>
                <div class="col-md-3 footer-info">
                    <div>Developed by Invitious</div>
                </div>
            </div>
        </div>
    </div>
    <?php wp_footer();?>
</body>

</html>