<?php  require_once(get_theme_file_path() .'/inc/particials/navbar.php'); ?>

<div id="product" class="container">
    <div class="row">
        <?php
        global $woocommerce;
        $woocommerce->cart->add_to_cart(11);
        global  $post;
        $_pf = new WC_Product_Factory();
        $_product = $_pf->get_product($post->ID);
        $image_url = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );?>
        <div class="col-lg-5">
            <div id="product-title"><?php echo $_product->name;?></div>
            <div id="product-location"><?php echo  get_post_meta($post->ID, 'origin')[0]?></div>
            <div id="product-price">$<?php echo $_product->price;?></div>
            <div id="product-description"><?php echo $_product->description;?></div>

            <div class="info-line">
                <span class="name">Origin</span>
                <span class="value"><?php echo  get_post_meta($post->ID, 'origin')[0]?></span>
            </div>
            <div class="info-line">
                <span class="name">Year</span>
                <span class="value"><?php echo  get_post_meta($post->ID, 'year')[0]?></span>
            </div>
            <div class="info-line">
                <span class="name">Varietal</span>
                <span class="value"><?php echo  get_post_meta($post->ID, 'varietal')[0]?></span>
            </div>
            <div class="info-line">
                <span class="name">Style</span>
                <span class="value"><?php echo  get_post_meta($post->ID, 'style')[0]?></span>
            </div>

            <div id="actions">
                <div id="minus-sign" class="quantity-buttons">-</div>
                <input type="number" id="quantity" max="50" value="1">
                <div id="plus-sign" class="quantity-buttons">+</div>

                <div id="add-button" onclick="collection_add_to_cart(<?php echo $post->ID;?>)">Add to your bag</div>
            </div>

        </div>
        <div class="col-lg-7">
            <img id="product-img" src="<?php echo $image_url[0];?>" draggable="false">
        </div>
    </div>
</div>

<script>
    function collection_add_to_cart(id) {

        var data = {
            action: 'add_item',
            product_id: id,
            quantity: $('#quantity').val()
        };

        jQuery.post('<?php echo get_site_url(); ?>/wp-admin/admin-ajax.php' , data, function(response) {
            var data = JSON.parse(response);
            if(data.status != 0) {
                $('#cart-number').html(data.cart_count);
            }
        });


        return false;
    }
</script>

<?php  require_once(get_theme_file_path() .'/inc/particials/footer.php'); ?>

