<?php include "inc/particials/navbar.php"; ?>


<?php
if ( have_posts() ) {

    if (is_front_page()) {
        while (have_posts()) : the_post();
            get_template_part('templates/content-front-page');
        endwhile;
    } else {

        while (have_posts()) : the_post();

            get_template_part('templates/content-subpages');
        endwhile;
    }

} else {

    get_template_part('template-parts/post/content', 'none');
}

?>


<?php include "inc/particials/footer.php"; ?>