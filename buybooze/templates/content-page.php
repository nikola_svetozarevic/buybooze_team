<div class="subpage" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="container warranty-returns">
        <div class="row">
            <div class="col-md-8">
                <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>

                <div class="entry-content">
                    <?php
                    the_content();
                    ?>
                </div><!-- .entry-content -->
            </div>

            <div class="col-md-4">
                Side menu
            </div>
        </div>
    </div>
</div>