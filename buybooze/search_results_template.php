<?php
/**
 * Template Name: Search results
 */
require_once('inc/particials/navbar.php'); ?>

<?php
$serch_term = $_GET['product_name'];
    $args = [
        'post_type'         => 'product',
        'post_status'       => 'publish',
        'posts_per_page'    => 999,
        's'      => $serch_term,
    ];
    $loop = new WP_Query($args);
?>
<div id="search-results">
    <div class="container">
        <div class="row">

            <!-- Search result  -->
            <?php
            if ($loop->have_posts()) {
                while ( $loop->have_posts() ) : $loop->the_post();
                    global $product;
            ?>
                    <div class="col-sm-3 article">
                        <div class="article-image" style="background-image: url('<?php echo wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID), 'single-post-thumbnail')[0];?>');"></div>
                        <div class="row">
                            <a href="<?php echo get_permalink( $post->ID );?>?>" class="col-xs-8 article-name"><?php echo $product->name;?></a>
                            <div class="col-xs-4 article-price"><?php echo $product->price;?></div>
                        </div>
                        <div class="article-desc"><?php echo $product->description;?></div>
                    </div>
            <?php
                endwhile;
            } else {
                echo '<h1>No search result found.</h1>';
            }
            ?>

            <!-- Search result end  -->

        </div>
    </div>
</div>

<?php require_once('inc/particials/footer.php'); ?>
