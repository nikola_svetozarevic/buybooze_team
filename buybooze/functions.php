<?php
class BuyBooze
{
    public function __construct()
    {

    }
}
$buyBooze = new BuyBooze();

function add_styles_and_scripts() {
    wp_enqueue_style( 'bootstrap.css', get_template_directory_uri() . '/assets/css/bootstrap.css' );
    wp_enqueue_style( 'footer.css', get_stylesheet_directory_uri() . '/assets/css/main.css' );

    wp_enqueue_script( 'jquery.js', get_stylesheet_directory_uri() . '/assets/js/jquery-3.2.1.js', [], '1.0.0', true);
    wp_enqueue_script( 'bootstrap.js', get_stylesheet_directory_uri() . '/assets/js/bootstrap.js', ['jquery.js'], '1.0', true );
    wp_enqueue_script( 'npm.js', get_stylesheet_directory_uri() . '/assets/js/npm.js', ['jquery.js'], '1.0', true );
    wp_enqueue_script( 'index.js', get_stylesheet_directory_uri() . '/assets/js/main.js', ['jquery.js', 'bootstrap.js', 'npm.js'], '1.0', true );
}

add_action( 'wp_enqueue_scripts', 'add_styles_and_scripts' );

add_action( 'woocommerce_product_options_general_product_data', 'woo_add_custom_fields');
add_action( 'woocommerce_process_product_meta', 'woo_save_custom_fields');

function woo_add_custom_fields() {
    echo '<div class="options-group">';
    woocommerce_wp_text_input([
        'id'            => 'origin',
        'label'         => __( 'Origin', 'woocommerce' ),
        'placeholder'   => 'Origin',
        'desc_tip'      => 'true',
        'description'   => __('Enter products origin', 'woocommerce')
    ]);
    woocommerce_wp_text_input([
        'id'            => 'year',
        'label'         => __( 'Year', 'woocommerce' ),
        'placeholder'   => 'Year',
        'desc_tip'      => 'true',
        'description'   => __('Enter products year', 'woocommerce')
    ]);
    woocommerce_wp_text_input([
        'id'            => 'varietal',
        'label'         => __( 'Varietal', 'woocommerce' ),
        'placeholder'   => 'Varietal',
        'desc_tip'      => 'true',
        'description'   => __('Enter products varietal', 'woocommerce')
    ]);
    woocommerce_wp_text_input([
        'id'            => 'style',
        'label'         => __( 'Style', 'woocommerce' ),
        'placeholder'   => 'Style',
        'desc_tip'      => 'true',
        'description'   => __('Enter products style', 'woocommerce')
    ]);
    echo '</div>';
}

function woo_save_custom_fields($post_id)
{
    $origin = $_POST['origin'];
    update_post_meta($post_id, 'origin', esc_attr($origin));

    $origin = $_POST['year'];
    update_post_meta($post_id, 'year', esc_attr($origin));

    $origin = $_POST['varietal'];
    update_post_meta($post_id, 'varietal', esc_attr($origin));

    $origin = $_POST['style'];
    update_post_meta($post_id, 'style', esc_attr($origin));
}

function ajax_add_item()
{

    $product_id = intval($_POST['product_id']);
    $quantity = intval($_POST['quantity']);
    global $woocommerce;
    $woocommerce->cart->add_to_cart($product_id, $quantity);
   // WC()->cart->add_to_cart(11, 1);
    $cart_count = WC()->cart->get_cart_contents_count();

    $response = [
        "status" => 1,
        "cart_count" => $cart_count
    ];


    echo json_encode($response);
    die();
}

add_action('wp_ajax_add_item', 'ajax_add_item');
add_action('wp_ajax_nopriv_add_item', 'ajax_add_item');


function title_filter($where, &$wp_query){
    global $wpdb;

    if($search_term = $wp_query->get( 'search_prod_title' )){
        /*using the esc_like() in here instead of other esc_sql()*/
        $search_term = $wpdb->esc_like($search_term);
        $search_term = ' \'%' . $search_term . '%\'';
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE '.$search_term;
    }

    return $where;
}

function theme_setup () {
    register_nav_menus(
        [
            'header-menu' => __('Header Menu')
        ]
    );
}

function empty_cart_redirect() {
    return get_site_url();
}

add_action('init', 'theme_setup');

add_filter('woocommerce_return_to_shop_redirect', 'empty_cart_redirect');