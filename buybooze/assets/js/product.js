var MAX_QUANTITY_VALUE = 50;

$('#plus-sign').on('click',function() {
    if ($('#quantity').val() < MAX_QUANTITY_VALUE) $('#quantity').get(0).value++;
});

$('#minus-sign').on('click',function() {
    if ($('#quantity').val() > 0) $('#quantity').get(0).value--;
});

$('#quantity').on('keyup', function() {
    if ($('#quantity').val() > MAX_QUANTITY_VALUE) $(this).val(MAX_QUANTITY_VALUE);
});

//$("#add-button").on('click', addProductToBag);

/*
function addProductToBag() {
    // TODO: PLACE CODE FOR ADDING PRODUCT TO A SHOPPING BAG
    alert(`Product added: \n\n${$('#title').html()}\nQuantity: ${$('#quantity').val()}`);
}*/
