var maxNavbarWidth = 0;
var maxNavbarHeight = 0;
var navbar = $('#navbar');
var navbarContainer = $('#navbar-container');
var logo = $('#logo');
var linkWrapper = $('#link-wrapper');
var isCollapsed = true;
var mobileView = false;

function initNavbar() {
    $('.navbar-link').each(function () {
        maxNavbarWidth += Math.ceil($(this).outerWidth());
        maxNavbarHeight += $(this).outerHeight();
    });
    maxNavbarWidth += Math.ceil($('#logo').outerWidth(true));
    linkWrapper.css('opacity', '1'); // to prevent flickering when load on mobile
}

function isMobile() {
    return navbarContainer.innerWidth() < maxNavbarWidth + 30;
}

function responsiveNavbar() {
    if (isMobile()) {
        if (mobileView) return;
        linkWrapper.addClass('hidden');
        linkWrapper.addClass('mobile-links');
        logo.addClass('mobile');
        mobileView = true;
    } else {
        collaps(true);
        linkWrapper.removeClass('hidden');
        linkWrapper.removeClass('mobile-links');
        logo.removeClass('mobile');
        mobileView = false;
    }
}

function collaps(collaps) {
    if (collaps) {
        linkWrapper.addClass('hidden');
        linkWrapper.css('top', `auto`);
        linkWrapper.css('height', `auto`);
    } else {
        linkWrapper.removeClass('hidden');
        linkWrapper.css('top', `${navbar.outerHeight()}px`);
        linkWrapper.css('height', `${maxNavbarHeight}px`);
    }
    isCollapsed = collaps;
}

$('#logo').on('click', function () {
    if ($(this).hasClass('mobile')) {
        collaps(!isCollapsed);
    }
});

window.addEventListener("load", function () {
    initNavbar();
    responsiveNavbar();
}, false);

window.addEventListener("resize", function () {
    responsiveNavbar();
}, false);