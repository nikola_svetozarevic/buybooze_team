function initIndex() {
    $('#big-image').css('height', window.innerHeight - navbar.outerHeight() - 40);
    $('#big-image').css('min-height', $('#big-image').outerWidth() / 2);
    $('.article-image').css('height', $('.article-image').outerWidth());
}

window.addEventListener("load", function () {
    initIndex();
}, false);

window.addEventListener("resize", function () {
    initIndex();
}, false);
