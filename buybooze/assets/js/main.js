var maxNavbarWidth = 0;
var maxNavbarHeight = 0;
var navbar = $('#navbar');
var navbarContainer = $('#navbar-container');
var logo = $('#logo');
var linkWrapper = $('#link-wrapper');
var isCollapsed = true;
var mobileView = false;
var MAX_QUANTITY_VALUE = 50;
var MOB_NAVBAR_ITEM_HEIGHT = 52;

var navigation = function () {
    function prepareLinks() {
        $('.navbar-link').each(function () {
            maxNavbarWidth += Math.ceil($(this).outerWidth());
            // maxNavbarHeight += Math.ceil($(this).outerHeight());
            maxNavbarHeight += MOB_NAVBAR_ITEM_HEIGHT;
            console.log(`navbar-links (${$(this)}): ${Math.ceil($(this).outerHeight())}`);
        });
        $('#link-wrapper > .menu-header-menu-container > ul > li').each(function () {
            maxNavbarWidth += Math.ceil($(this).outerWidth());
            // maxNavbarHeight += Math.ceil($(this).outerHeight());
            maxNavbarHeight += MOB_NAVBAR_ITEM_HEIGHT;
            console.log(`menu-item (${$(this)}): ${Math.ceil($(this).outerHeight())}`);
        });
        maxNavbarWidth += Math.ceil($('#logo').outerWidth(true));
        console.log(`logo: ${maxNavbarWidth}`);
        linkWrapper.css('opacity', '1'); // to prevent flickering during load on mobile
    }

    function isMobile() {
        return navbarContainer.innerWidth() < maxNavbarWidth + 30;
    }

    function check() {
        if (isMobile()) {
            if (mobileView) return;
            linkWrapper.addClass('hidden');
            linkWrapper.addClass('mobile-links');
            logo.addClass('mobile');
            mobileView = true;
        } else {
            collaps(true);
            linkWrapper.removeClass('hidden');
            linkWrapper.removeClass('mobile-links');
            logo.removeClass('mobile');
            mobileView = false;
        }
    }

    function collaps(collaps) {
        if (!$('#logo').hasClass('mobile')) return;
        if (collaps) {
            linkWrapper.addClass('hidden');
            linkWrapper.css('top', `auto`);
            linkWrapper.css('height', `auto`);
        } else {
            linkWrapper.removeClass('hidden');
            linkWrapper.css('top', `${navbar.outerHeight()}px`);
            linkWrapper.css('height', `${maxNavbarHeight}px`);
        }
        isCollapsed = collaps;
    }

    return {
        prepareLinks,
        check,
        collaps
    };
}();

var search = function () {
    function show(visible) {
        if (visible) {
            navigation.collaps(true);
            $('#search').addClass('show');
            $('#search-input').focus();
            $('body').addClass('preventScroll');
            addBlur();
        } else {
            $('#search').removeClass('show');
            $('body').removeClass('preventScroll');
            addBlur(false);
        }
    }
    function addBlur(state = true) {
        if (state) {
            $('body').children('div').each(function() {
                if ($(this).attr('id') !== 'search') {
                    $(this).addClass('blur');
                }
            });
        } else {
            $('body').children('div').each(function() {
                if ($(this).attr('id') !== 'search') {
                    $(this).removeClass('blur');
                }
            });
        }
    }
    function getProduct(data) {
        $('#search-form').submit();
    }
    return {
        show,
        getProduct
    };
}();

$('#logo').on('click', function () {
    navigation.collaps(!isCollapsed);
});

$('#search-button').on('click', function () {
    search.show(true);
});

$('#search-close-button').on('click', function () {
    search.show(false);
});

$("#search-input").keyup(function (event) {
    let value = $(this).val();
    if (event.keyCode === 13) {
        search.getProduct(value);
    }
});

// Index

function initIndex() {
    $('#big-image').css('height', window.innerHeight - navbar.outerHeight() - 40);
    $('#big-image').css('min-height', $('#big-image').outerWidth() / 2);
    $('.article-image').css('height', $('.article-image').outerWidth());
}

// Product

$('#plus-sign').on('click',function() {
    if ($('#quantity').val() < MAX_QUANTITY_VALUE) $('#quantity').get(0).value++;
});

$('#minus-sign').on('click',function() {
    if ($('#quantity').val() > 0) $('#quantity').get(0).value--;
});

$('#quantity').on('keyup', function() {
    if ($('#quantity').val() > MAX_QUANTITY_VALUE) $(this).val(MAX_QUANTITY_VALUE);
});

//$("#add-button").on('click', addProductToBag);

/*function addProductToBag() {
    // TODO: PLACE CODE FOR ADDING PRODUCT TO A SHOPPING BAG
    var cartCounter = parseInt($('#cart-number').html());
    $('#cart-number').html(cartCounter + 1);
    alert(`Product added: \n\n${$('#title').html()}\nQuantity: ${$('#quantity').val()}`);
}*/

// Search

function initSearch() {
    $('.article-image').css('height', $('.article-image').outerWidth());
}

// Footer

function checkFooter() {
    var windowHeight = window.innerHeight;
    var footerHeight = document.getElementById('footer').offsetHeight;
    var bodyHeight = $('#footer').hasClass('fixed') ? document.body.offsetHeight + footerHeight : document.body.offsetHeight;
    
    if (windowHeight > bodyHeight) {
        $('#footer').addClass('fixed');
    } else {
        $('#footer').removeClass('fixed');
    }
}

// Events

window.addEventListener("load", function () {
    navigation.prepareLinks();
    navigation.check();
    initIndex();
    initSearch();
    checkFooter();
    $("#loadingScreen").addClass('removed');
}, false);

window.addEventListener("resize", function () {
    navigation.check();
    initIndex();
    initSearch();
    checkFooter();
}, false);